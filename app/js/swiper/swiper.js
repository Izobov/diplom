var mySwiper = new Swiper('.swiper-container', {
    effect: "fade",
    loop: true,
    speed: 1500,

    autoplay: {
        delay: 3000,
    },
})