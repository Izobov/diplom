var mobile_menu = document.querySelector('.js_mobile_menu');
var navigation = document.querySelector('.mobile_menu');
var close = document.querySelector('.js_close');
var portfolio = document.querySelector('.js_portfolio');
var portfolio_ul = document.querySelector('.js_portfolio_ul');
var info = document.querySelector('.js_info');
var info_ul = document.querySelector('.js_info_ul');
var order_buttom = document.querySelector('.js_order_buttom');
var popup = document.querySelector('.popup');
var close_popup = document.querySelector('.js_close_popup');

mobile_menu.addEventListener('click', function () {
    navigation.classList.add('show')
});

close.addEventListener('click', function () {
    navigation.classList.remove('show')
});

portfolio.addEventListener('click', function () {
    portfolio_ul.classList.toggle('show');
});

info.addEventListener('click', function () {
    info_ul.classList.toggle('show');
});

order_buttom.addEventListener('click', function () {
    popup.classList.add('show')
});

close_popup.addEventListener('click', function () {
    popup.classList.remove('show')
});